const mongoose = require("mongoose");
const config = require("./config");


mongoose.connect("mongodb://localhost/" + config.get("db-name-demo"));

const db = mongoose.connection;
console.log(db.readyState);
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", function() {
  console.log("connected!");
  console.log(db.readyState);
});

const kittySchema = mongoose.Schema({
  name: String
});
//We've got a schema with one property, name, which will be a String. The next step is compiling our schema into a Model.
// let Kitten = mongoose.model("Kitten", kittySchema);
//
// let silence = new Kitten({ name: "Silence" });
// console.log(silence.name); // 'Silence'

// NOTE: methods must be added to the schema before compiling it with mongoose.model()
kittySchema.methods.speak = function () {
  let greeting = this.name
    ? "Meow name is " + this.name
    : "I don't have a name";
  console.log(greeting);
};

let KittenNew = mongoose.model("Kitten", kittySchema);

let fluffy = new KittenNew({ name: "fluffy" });
fluffy.speak(); // "Meow name is fluffy"

fluffy.save(function (err, data, affected) {
  console.log(arguments);
});
