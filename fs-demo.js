const fs = require("fs");

fs.writeFile("file.tmp", "demo file string", err => {
  if (err) throw err;

  fs.unlink("file.tmp", err => {
    if (err) throw err;
  });

});