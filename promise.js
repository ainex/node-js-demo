"use strict";

let promise = new Promise(((resolve, reject) => {
  setTimeout(function () {
      resolve("result 1");
    },
    1000);
}));

promise.then(result => {
  console.log("got result: " + result);
  return "result 2";

}, (error) => {
  console.log("error" + error);
})
  .then(result => {
    console.log("got result: " + result);
    console.log("returning new promise");
    return new Promise(resolve => {

      setTimeout(function () {
        console.log("in new promise");
        resolve("new promise result 3");
      }, 3000);
    });

  })
  .then(result => {
    console.log("last one result: " + result);
  });

