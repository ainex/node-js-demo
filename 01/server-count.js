const {Server} = require("http");
const handle = require("./request-handler");

// let i = 0;

// const server = new Server((req, res) => {
//
//   i += 10;
//   res.end(i.toString()); // (!!! toString) Buffer
// });
const server = new Server((req, res) => handle(req, res));

server.listen(8000);

