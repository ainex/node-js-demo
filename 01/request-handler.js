module.exports = handler;

let counter = 0;
function handler (req, res) {
  ++counter;
  res.end(counter.toString());
}