/**
 ЗАДАЧА - научиться работать с потоками (streams)
 Написать HTTP-сервер для загрузки и получения файлов
 - Все файлы находятся в директории files
 - Структура файлов НЕ вложенная.

 - Виды запросов к серверу
 GET /file.ext
 - выдаёт файл file.ext из директории files,

 POST /file.ext
 - пишет всё тело запроса в файл files/file.ext и выдаёт ОК
 - если файл уже есть, то выдаёт ошибку 409
 - при превышении файлом размера 1MB выдаёт ошибку 413

 DELETE /file
 - удаляет файл
 - выводит 200 OK
 - если файла нет, то ошибка 404

 Вместо file может быть любое имя файла.
 Так как поддиректорий нет, то при наличии / или .. в пути сервер должен выдавать ошибку 400.

 - Сервер должен корректно обрабатывать ошибки "файл не найден" и другие (ошибка чтения файла)
 - index.html или curl для тестирования

 */

// Пример простого сервера в качестве основы

"use strict";

const url = require("url");
const fs = require("fs");


require("http").createServer(function (req, res) {

  let pathname = decodeURI(url.parse(req.url).pathname);

  switch (req.method) {
    case "GET":
      if (pathname == "/") {
        let fileStream = fs.ReadStream(__dirname + "/public/index.html");
        sendFile(fileStream, res);
        return;
      } else {
        res.end("server error");
      }
      break;

    default:
      res.statusCode = 502;
      res.end("Not implemented");
  }

}).listen(3000);

function sendFile(fileStream, res) {
  //or using pipe
  // fileStream.pipe(res);

  fileStream.on("readable", write);

  function write() {
    let data = fileStream.read();
    console.log("readable");
    //res.write(data)
    // Returns true if the entire data was flushed successfully to the kernel buffer.
    // Returns false if all or part of the data was queued in user memory. 'drain' will be emitted when the buffer is free again.
    if (data && !res.write(data)) {
      //full buffer, suspend writing
      fileStream.removeListener("readable", write);
      res.once("drain", function () {
        //buffer is free, resume writing
        fileStream.on("readable", write);
        write();
      });
    }
  }

  fileStream.on("end", function () {
    console.log("end");
    res.end();
  });

  fileStream.on("open", function () {
    console.log("open file");

  })
    .on("close", function () {
      console.log("close file");

    });

  fileStream.on("error", function (err) {
    // if (error.code == "ENOENT") {
    //   console.log("file not found");
    // } else {
    //   console.error(error);
    // }
    res.statusCode = 500;
    res.end("server error");
    console.error(err);
  });

  //connection was dropped on client (not finish)
  res.on("close", function () {
    fileStream.destroy();
  });
}


