const log = require("./log")("app.js");

const http = require("http");
const url = require("url");
console.log("demo node js");

http.createServer((request, response) => {
  let ulrParsed = url.parse(request.url, true);
  console.log(request.method, request.headers);
  console.log(ulrParsed);
  response.setHeader("Content-Type","text/plain");
  if (ulrParsed.pathname == "/echo" && ulrParsed.query.message) {
    response.statusCode = 200;
    log.info("fff");
    response.end(ulrParsed.query.message);
  } else {
    response.statusCode = 404;
    response.end("page not found");
  }

}).listen("8888");
